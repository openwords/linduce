<?php
 
class Gala_Galawintertimesettings_Model_Config_Boxwide
{
	public function toOptionArray()
    {
		return array(
			array('label' => "Box", 'value' => "box"),
			array('label' => "Wide", 'value' => "wide")
		);
    }
 
}
