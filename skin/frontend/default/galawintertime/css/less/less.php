<?php
    return array(
        'screen-large-desktop' => '1200px',
        'screen-desktop' => '992px',
        'screen-tablet' => '768px',
              
    	'page_bg_color' => '#8F9AD8',
        'page_bg_position' => "0 0",
    	'page_bg_repeat' => 'repeat',
        'box_shadow' => 'transparent',
    	'rounded_corner' => '0',
        
    	'general_font' => "13px/1.35 Montserrat, Arial, sans-serif",
    	'h1_font' => "24px Montserrat, Arial, san-serif",
    	'h2_font' => "20px Montserrat, Arial, san-serif",
    	'h3_font' => "18px Montserrat, Arial, san-serif",
    	'h4_font' => "16px Montserrat, Arial, san-serif",
    	'h5_font' => "14px Montserrat, Arial, san-serif",     
    	'h6_font' => "12px Montserrat, Arial, san-serif",     
    	'slider_title_font' => "36px Montserrat, Arial, Times New Roman, serif",     
        
        'header_bg_color' => '#fff',
    	'header_bg_position' => "0 0",
    	'header_bg_repeat' => 'repeat',
    	'header_text_color' => '#818180',
        'header_text2_color' => '#030303',//text link hover	
		'header_text3_color' => '#191919',// text link
		'header_line_color' => '#dfe1e1',//line search dropdown
        'header_line2_color' => '#000', //border dropdown
		        
		'topmenu_bg_color' => '#fff',
    	'topmenu_text_color' => '#818180',
    	'topmenu_hover_text_color' => '#030303',
        
    	'dropmenu_bg_color' => '#fff',    	
    	'dropmenu_text_color' => '#000',
        'dropmenu_text2_color' => '#767575 ',
        'dropmenu_line_color' => '#dfe1e1',
        
        'body_bg_color' => '#fff',
        'body_bg2_color' => '#000',// bg title  
        'body_bg3_color' => '#367ea4',// bg about  
		'body_bg_position' => "0 0",
    	'body_bg_repeat' => "repeat",
        'body_text_color' => '#367fa4',
		'body_text2_color' => '#202122',//price, title
        'body_text3_color' => '#767575',//product name.	
        'body_text4_color' => '#c3c3c3',	//title tab inactive.
        'body_text5_color' => '#fff',
        'body_line_color' => '#ccc',// table line
        'body_line4_color' => '#dfe1e1',//border block,input, 
		//lighten(@body_line_color, 20%) #ccc
        
        'footer_bg_color' => '#fff',
    	'footer_bg_position' => '0 0',
    	'footer_bg_repeat' => 'repeat',
        'footer_text_color' => '#367fa4',
        'footer_text2_color' => '#000',
        'footer_text3_color' => '#8c8a88',
        'footer_text4_color' => '#585757',
        'footer_line_color' => '#000',//changed subscribe
        'footer_line2_color' => '#ccc',
		
        'slider_bg_color' => 'transparent',     
    	'slider_bg_position' => "0 0",
    	'slider_bg_repeat' => "no-repeat",
                
        'btn1_bg_color' => '#000',
        'btn1_hover_bg_color' => '#357fa4',//delete
        'btn1_line_color' => '#000',
    	'btn1_text_color' => '#fff',
        
        'btn2_bg_color' => '#fff',
    	'btn2_text_color' => '#000',
    	'btn2_line_color' => '#000',
    	'btn2_hover_line_color' => '#367fa4',//delete
        
        'btn3_bg_color' => '#367ea4',//btn-checkout
        'btn3_hover_bg_color' => '#000',
    	'btn3_text_color' => '#fff',
    );
?>