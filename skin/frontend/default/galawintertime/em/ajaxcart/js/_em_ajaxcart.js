/*!
 * jQuery Form Plugin
 * version: 3.51.0-2014.06.20
 * Requires jQuery v1.5 or later
 * Copyright (c) 2014 M. Alsup
 * Examples and documentation at: http://malsup.com/jquery/form/
 * Project repository: https://github.com/malsup/form
 * Dual licensed under the MIT and GPL licenses.
 * https://github.com/malsup/form#copyright-and-license
 */
!function(e){"use strict";"function"==typeof define&&define.amd?define(["jquery"],e):e("undefined"!=typeof jQuery?jQuery:window.Zepto)}(function(e){"use strict";function t(t){var r=t.data;t.isDefaultPrevented()||(t.preventDefault(),e(t.target).ajaxSubmit(r))}function r(t){var r=t.target,a=e(r);if(!a.is("[type=submit],[type=image]")){var n=a.closest("[type=submit]");if(0===n.length)return;r=n[0]}var i=this;if(i.clk=r,"image"==r.type)if(void 0!==t.offsetX)i.clk_x=t.offsetX,i.clk_y=t.offsetY;else if("function"==typeof e.fn.offset){var o=a.offset();i.clk_x=t.pageX-o.left,i.clk_y=t.pageY-o.top}else i.clk_x=t.pageX-r.offsetLeft,i.clk_y=t.pageY-r.offsetTop;setTimeout(function(){i.clk=i.clk_x=i.clk_y=null},100)}function a(){if(e.fn.ajaxSubmit.debug){var t="[jquery.form] "+Array.prototype.join.call(arguments,"");window.console&&window.console.log?window.console.log(t):window.opera&&window.opera.postError&&window.opera.postError(t)}}var n={};n.fileapi=void 0!==e("<input type='file'/>").get(0).files,n.formdata=void 0!==window.FormData;var i=!!e.fn.prop;e.fn.attr2=function(){if(!i)return this.attr.apply(this,arguments);var e=this.prop.apply(this,arguments);return e&&e.jquery||"string"==typeof e?e:this.attr.apply(this,arguments)},e.fn.ajaxSubmit=function(t){function r(r){var a,n,i=e.param(r,t.traditional).split("&"),o=i.length,s=[];for(a=0;o>a;a++)i[a]=i[a].replace(/\+/g," "),n=i[a].split("="),s.push([decodeURIComponent(n[0]),decodeURIComponent(n[1])]);return s}function o(a){for(var n=new FormData,i=0;i<a.length;i++)n.append(a[i].name,a[i].value);if(t.extraData){var o=r(t.extraData);for(i=0;i<o.length;i++)o[i]&&n.append(o[i][0],o[i][1])}t.data=null;var s=e.extend(!0,{},e.ajaxSettings,t,{contentType:!1,processData:!1,cache:!1,type:u||"POST"});t.uploadProgress&&(s.xhr=function(){var r=e.ajaxSettings.xhr();return r.upload&&r.upload.addEventListener("progress",function(e){var r=0,a=e.loaded||e.position,n=e.total;e.lengthComputable&&(r=Math.ceil(a/n*100)),t.uploadProgress(e,a,n,r)},!1),r}),s.data=null;var c=s.beforeSend;return s.beforeSend=function(e,r){r.data=t.formData?t.formData:n,c&&c.call(this,e,r)},e.ajax(s)}function s(r){function n(e){var t=null;try{e.contentWindow&&(t=e.contentWindow.document)}catch(r){a("cannot get iframe.contentWindow document: "+r)}if(t)return t;try{t=e.contentDocument?e.contentDocument:e.document}catch(r){a("cannot get iframe.contentDocument: "+r),t=e.document}return t}function o(){function t(){try{var e=n(g).readyState;a("state = "+e),e&&"uninitialized"==e.toLowerCase()&&setTimeout(t,50)}catch(r){a("Server abort: ",r," (",r.name,")"),s(k),j&&clearTimeout(j),j=void 0}}var r=f.attr2("target"),i=f.attr2("action"),o="multipart/form-data",c=f.attr("enctype")||f.attr("encoding")||o;w.setAttribute("target",p),(!u||/post/i.test(u))&&w.setAttribute("method","POST"),i!=m.url&&w.setAttribute("action",m.url),m.skipEncodingOverride||u&&!/post/i.test(u)||f.attr({encoding:"multipart/form-data",enctype:"multipart/form-data"}),m.timeout&&(j=setTimeout(function(){T=!0,s(D)},m.timeout));var l=[];try{if(m.extraData)for(var d in m.extraData)m.extraData.hasOwnProperty(d)&&l.push(e.isPlainObject(m.extraData[d])&&m.extraData[d].hasOwnProperty("name")&&m.extraData[d].hasOwnProperty("value")?e('<input type="hidden" name="'+m.extraData[d].name+'">').val(m.extraData[d].value).appendTo(w)[0]:e('<input type="hidden" name="'+d+'">').val(m.extraData[d]).appendTo(w)[0]);m.iframeTarget||v.appendTo("body"),g.attachEvent?g.attachEvent("onload",s):g.addEventListener("load",s,!1),setTimeout(t,15);try{w.submit()}catch(h){var x=document.createElement("form").submit;x.apply(w)}}finally{w.setAttribute("action",i),w.setAttribute("enctype",c),r?w.setAttribute("target",r):f.removeAttr("target"),e(l).remove()}}function s(t){if(!x.aborted&&!F){if(M=n(g),M||(a("cannot access response document"),t=k),t===D&&x)return x.abort("timeout"),void S.reject(x,"timeout");if(t==k&&x)return x.abort("server abort"),void S.reject(x,"error","server abort");if(M&&M.location.href!=m.iframeSrc||T){g.detachEvent?g.detachEvent("onload",s):g.removeEventListener("load",s,!1);var r,i="success";try{if(T)throw"timeout";var o="xml"==m.dataType||M.XMLDocument||e.isXMLDoc(M);if(a("isXml="+o),!o&&window.opera&&(null===M.body||!M.body.innerHTML)&&--O)return a("requeing onLoad callback, DOM not available"),void setTimeout(s,250);var u=M.body?M.body:M.documentElement;x.responseText=u?u.innerHTML:null,x.responseXML=M.XMLDocument?M.XMLDocument:M,o&&(m.dataType="xml"),x.getResponseHeader=function(e){var t={"content-type":m.dataType};return t[e.toLowerCase()]},u&&(x.status=Number(u.getAttribute("status"))||x.status,x.statusText=u.getAttribute("statusText")||x.statusText);var c=(m.dataType||"").toLowerCase(),l=/(json|script|text)/.test(c);if(l||m.textarea){var f=M.getElementsByTagName("textarea")[0];if(f)x.responseText=f.value,x.status=Number(f.getAttribute("status"))||x.status,x.statusText=f.getAttribute("statusText")||x.statusText;else if(l){var p=M.getElementsByTagName("pre")[0],h=M.getElementsByTagName("body")[0];p?x.responseText=p.textContent?p.textContent:p.innerText:h&&(x.responseText=h.textContent?h.textContent:h.innerText)}}else"xml"==c&&!x.responseXML&&x.responseText&&(x.responseXML=X(x.responseText));try{E=_(x,c,m)}catch(y){i="parsererror",x.error=r=y||i}}catch(y){a("error caught: ",y),i="error",x.error=r=y||i}x.aborted&&(a("upload aborted"),i=null),x.status&&(i=x.status>=200&&x.status<300||304===x.status?"success":"error"),"success"===i?(m.success&&m.success.call(m.context,E,"success",x),S.resolve(x.responseText,"success",x),d&&e.event.trigger("ajaxSuccess",[x,m])):i&&(void 0===r&&(r=x.statusText),m.error&&m.error.call(m.context,x,i,r),S.reject(x,"error",r),d&&e.event.trigger("ajaxError",[x,m,r])),d&&e.event.trigger("ajaxComplete",[x,m]),d&&!--e.active&&e.event.trigger("ajaxStop"),m.complete&&m.complete.call(m.context,x,i),F=!0,m.timeout&&clearTimeout(j),setTimeout(function(){m.iframeTarget?v.attr("src",m.iframeSrc):v.remove(),x.responseXML=null},100)}}}var c,l,m,d,p,v,g,x,y,b,T,j,w=f[0],S=e.Deferred();if(S.abort=function(e){x.abort(e)},r)for(l=0;l<h.length;l++)c=e(h[l]),i?c.prop("disabled",!1):c.removeAttr("disabled");if(m=e.extend(!0,{},e.ajaxSettings,t),m.context=m.context||m,p="jqFormIO"+(new Date).getTime(),m.iframeTarget?(v=e(m.iframeTarget),b=v.attr2("name"),b?p=b:v.attr2("name",p)):(v=e('<iframe name="'+p+'" src="'+m.iframeSrc+'" />'),v.css({position:"absolute",top:"-1000px",left:"-1000px"})),g=v[0],x={aborted:0,responseText:null,responseXML:null,status:0,statusText:"n/a",getAllResponseHeaders:function(){},getResponseHeader:function(){},setRequestHeader:function(){},abort:function(t){var r="timeout"===t?"timeout":"aborted";a("aborting upload... "+r),this.aborted=1;try{g.contentWindow.document.execCommand&&g.contentWindow.document.execCommand("Stop")}catch(n){}v.attr("src",m.iframeSrc),x.error=r,m.error&&m.error.call(m.context,x,r,t),d&&e.event.trigger("ajaxError",[x,m,r]),m.complete&&m.complete.call(m.context,x,r)}},d=m.global,d&&0===e.active++&&e.event.trigger("ajaxStart"),d&&e.event.trigger("ajaxSend",[x,m]),m.beforeSend&&m.beforeSend.call(m.context,x,m)===!1)return m.global&&e.active--,S.reject(),S;if(x.aborted)return S.reject(),S;y=w.clk,y&&(b=y.name,b&&!y.disabled&&(m.extraData=m.extraData||{},m.extraData[b]=y.value,"image"==y.type&&(m.extraData[b+".x"]=w.clk_x,m.extraData[b+".y"]=w.clk_y)));var D=1,k=2,A=e("meta[name=csrf-token]").attr("content"),L=e("meta[name=csrf-param]").attr("content");L&&A&&(m.extraData=m.extraData||{},m.extraData[L]=A),m.forceSync?o():setTimeout(o,10);var E,M,F,O=50,X=e.parseXML||function(e,t){return window.ActiveXObject?(t=new ActiveXObject("Microsoft.XMLDOM"),t.async="false",t.loadXML(e)):t=(new DOMParser).parseFromString(e,"text/xml"),t&&t.documentElement&&"parsererror"!=t.documentElement.nodeName?t:null},C=e.parseJSON||function(e){return window.eval("("+e+")")},_=function(t,r,a){var n=t.getResponseHeader("content-type")||"",i="xml"===r||!r&&n.indexOf("xml")>=0,o=i?t.responseXML:t.responseText;return i&&"parsererror"===o.documentElement.nodeName&&e.error&&e.error("parsererror"),a&&a.dataFilter&&(o=a.dataFilter(o,r)),"string"==typeof o&&("json"===r||!r&&n.indexOf("json")>=0?o=C(o):("script"===r||!r&&n.indexOf("javascript")>=0)&&e.globalEval(o)),o};return S}if(!this.length)return a("ajaxSubmit: skipping submit process - no element selected"),this;var u,c,l,f=this;"function"==typeof t?t={success:t}:void 0===t&&(t={}),u=t.type||this.attr2("method"),c=t.url||this.attr2("action"),l="string"==typeof c?e.trim(c):"",l=l||window.location.href||"",l&&(l=(l.match(/^([^#]+)/)||[])[1]),t=e.extend(!0,{url:l,success:e.ajaxSettings.success,type:u||e.ajaxSettings.type,iframeSrc:/^https/i.test(window.location.href||"")?"javascript:false":"about:blank"},t);var m={};if(this.trigger("form-pre-serialize",[this,t,m]),m.veto)return a("ajaxSubmit: submit vetoed via form-pre-serialize trigger"),this;if(t.beforeSerialize&&t.beforeSerialize(this,t)===!1)return a("ajaxSubmit: submit aborted via beforeSerialize callback"),this;var d=t.traditional;void 0===d&&(d=e.ajaxSettings.traditional);var p,h=[],v=this.formToArray(t.semantic,h);if(t.data&&(t.extraData=t.data,p=e.param(t.data,d)),t.beforeSubmit&&t.beforeSubmit(v,this,t)===!1)return a("ajaxSubmit: submit aborted via beforeSubmit callback"),this;if(this.trigger("form-submit-validate",[v,this,t,m]),m.veto)return a("ajaxSubmit: submit vetoed via form-submit-validate trigger"),this;var g=e.param(v,d);p&&(g=g?g+"&"+p:p),"GET"==t.type.toUpperCase()?(t.url+=(t.url.indexOf("?")>=0?"&":"?")+g,t.data=null):t.data=g;var x=[];if(t.resetForm&&x.push(function(){f.resetForm()}),t.clearForm&&x.push(function(){f.clearForm(t.includeHidden)}),!t.dataType&&t.target){var y=t.success||function(){};x.push(function(r){var a=t.replaceTarget?"replaceWith":"html";e(t.target)[a](r).each(y,arguments)})}else t.success&&x.push(t.success);if(t.success=function(e,r,a){for(var n=t.context||this,i=0,o=x.length;o>i;i++)x[i].apply(n,[e,r,a||f,f])},t.error){var b=t.error;t.error=function(e,r,a){var n=t.context||this;b.apply(n,[e,r,a,f])}}if(t.complete){var T=t.complete;t.complete=function(e,r){var a=t.context||this;T.apply(a,[e,r,f])}}var j=e("input[type=file]:enabled",this).filter(function(){return""!==e(this).val()}),w=j.length>0,S="multipart/form-data",D=f.attr("enctype")==S||f.attr("encoding")==S,k=n.fileapi&&n.formdata;a("fileAPI :"+k);var A,L=(w||D)&&!k;t.iframe!==!1&&(t.iframe||L)?t.closeKeepAlive?e.get(t.closeKeepAlive,function(){A=s(v)}):A=s(v):A=(w||D)&&k?o(v):e.ajax(t),f.removeData("jqxhr").data("jqxhr",A);for(var E=0;E<h.length;E++)h[E]=null;return this.trigger("form-submit-notify",[this,t]),this},e.fn.ajaxForm=function(n){if(n=n||{},n.delegation=n.delegation&&e.isFunction(e.fn.on),!n.delegation&&0===this.length){var i={s:this.selector,c:this.context};return!e.isReady&&i.s?(a("DOM not ready, queuing ajaxForm"),e(function(){e(i.s,i.c).ajaxForm(n)}),this):(a("terminating; zero elements found by selector"+(e.isReady?"":" (DOM not ready)")),this)}return n.delegation?(e(document).off("submit.form-plugin",this.selector,t).off("click.form-plugin",this.selector,r).on("submit.form-plugin",this.selector,n,t).on("click.form-plugin",this.selector,n,r),this):this.ajaxFormUnbind().bind("submit.form-plugin",n,t).bind("click.form-plugin",n,r)},e.fn.ajaxFormUnbind=function(){return this.unbind("submit.form-plugin click.form-plugin")},e.fn.formToArray=function(t,r){var a=[];if(0===this.length)return a;var i,o=this[0],s=this.attr("id"),u=t?o.getElementsByTagName("*"):o.elements;if(u&&!/MSIE [678]/.test(navigator.userAgent)&&(u=e(u).get()),s&&(i=e(':input[form="'+s+'"]').get(),i.length&&(u=(u||[]).concat(i))),!u||!u.length)return a;var c,l,f,m,d,p,h;for(c=0,p=u.length;p>c;c++)if(d=u[c],f=d.name,f&&!d.disabled)if(t&&o.clk&&"image"==d.type)o.clk==d&&(a.push({name:f,value:e(d).val(),type:d.type}),a.push({name:f+".x",value:o.clk_x},{name:f+".y",value:o.clk_y}));else if(m=e.fieldValue(d,!0),m&&m.constructor==Array)for(r&&r.push(d),l=0,h=m.length;h>l;l++)a.push({name:f,value:m[l]});else if(n.fileapi&&"file"==d.type){r&&r.push(d);var v=d.files;if(v.length)for(l=0;l<v.length;l++)a.push({name:f,value:v[l],type:d.type});else a.push({name:f,value:"",type:d.type})}else null!==m&&"undefined"!=typeof m&&(r&&r.push(d),a.push({name:f,value:m,type:d.type,required:d.required}));if(!t&&o.clk){var g=e(o.clk),x=g[0];f=x.name,f&&!x.disabled&&"image"==x.type&&(a.push({name:f,value:g.val()}),a.push({name:f+".x",value:o.clk_x},{name:f+".y",value:o.clk_y}))}return a},e.fn.formSerialize=function(t){return e.param(this.formToArray(t))},e.fn.fieldSerialize=function(t){var r=[];return this.each(function(){var a=this.name;if(a){var n=e.fieldValue(this,t);if(n&&n.constructor==Array)for(var i=0,o=n.length;o>i;i++)r.push({name:a,value:n[i]});else null!==n&&"undefined"!=typeof n&&r.push({name:this.name,value:n})}}),e.param(r)},e.fn.fieldValue=function(t){for(var r=[],a=0,n=this.length;n>a;a++){var i=this[a],o=e.fieldValue(i,t);null===o||"undefined"==typeof o||o.constructor==Array&&!o.length||(o.constructor==Array?e.merge(r,o):r.push(o))}return r},e.fieldValue=function(t,r){var a=t.name,n=t.type,i=t.tagName.toLowerCase();if(void 0===r&&(r=!0),r&&(!a||t.disabled||"reset"==n||"button"==n||("checkbox"==n||"radio"==n)&&!t.checked||("submit"==n||"image"==n)&&t.form&&t.form.clk!=t||"select"==i&&-1==t.selectedIndex))return null;if("select"==i){var o=t.selectedIndex;if(0>o)return null;for(var s=[],u=t.options,c="select-one"==n,l=c?o+1:u.length,f=c?o:0;l>f;f++){var m=u[f];if(m.selected){var d=m.value;if(d||(d=m.attributes&&m.attributes.value&&!m.attributes.value.specified?m.text:m.value),c)return d;s.push(d)}}return s}return e(t).val()},e.fn.clearForm=function(t){return this.each(function(){e("input,select,textarea",this).clearFields(t)})},e.fn.clearFields=e.fn.clearInputs=function(t){var r=/^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i;return this.each(function(){var a=this.type,n=this.tagName.toLowerCase();r.test(a)||"textarea"==n?this.value="":"checkbox"==a||"radio"==a?this.checked=!1:"select"==n?this.selectedIndex=-1:"file"==a?/MSIE/.test(navigator.userAgent)?e(this).replaceWith(e(this).clone(!0)):e(this).val(""):t&&(t===!0&&/hidden/.test(a)||"string"==typeof t&&e(this).is(t))&&(this.value="")})},e.fn.resetForm=function(){return this.each(function(){("function"==typeof this.reset||"object"==typeof this.reset&&!this.reset.nodeType)&&this.reset()})},e.fn.enable=function(e){return void 0===e&&(e=!0),this.each(function(){this.disabled=!e})},e.fn.selected=function(t){return void 0===t&&(t=!0),this.each(function(){var r=this.type;if("checkbox"==r||"radio"==r)this.checked=t;else if("option"==this.tagName.toLowerCase()){var a=e(this).parent("select");t&&a[0]&&"select-one"==a[0].type&&a.find("option").selected(!1),this.selected=t}})},e.fn.ajaxSubmit.debug=!1});

 
/*
 * ajaxcart javascript;
 * update date : 06/06/2014.
 */
	var em_box;
	var em_ajc_counter;
	function ajax_option(param)
	{
		var url = em_ajc_baseurl+"ajaxcart/opt/index/";
		em_box.open();
		new Ajax.Request(url, {
			parameters:param,
			onSuccess: function(res) {
				data = res.responseText.evalJSON();
				changeOptions(data);
			}
		});
	}

	function ajax_add(url,param)
	{
		if(typeof param == 'undefined'){
			param ='ajax_package_name=' + em_ajc_package_name
				+ '&ajax_layout=' + em_ajc_layout
				+ '&ajax_template=' + em_ajc_template
				+ '&ajax_skin=' + em_ajc_skin;
		}
		var link = url.replace('checkout','ajaxcart').replace('wishlist/index','ajaxcart/wishlist');
		var tmp		=	url.search("in_cart");		//	shopping cart page
		em_box.open();
		new Ajax.Request(link, {
			parameters:param,
			onSuccess: function(data) {
				html = data.responseText.evalJSON();
				if(html.options == 1)	changeOptions(html);
				else{
					if(tmp > 0 ) {		//	addtocart in shopping cart page
						window.location.href = em_ajc_baseurl+'checkout/cart/';
					}
					else{
						changeHTML(html);
					}
				}
			}
		});
	}

	function changeOptions(data){
		$('ajax_loading').hide();
		$('closeLink').show();
		$('viewcart_button').show();
		$('ajax_content').show();
		$('ajc_btn_checkout').hide();
		$$('#ajax_image').each(function (el){
			el.innerHTML = data.html;
		});
		data.html.evalScripts();
	}

	function changeHTML(html){
		$('ajax_loading').hide();
		$('closeLink').show();
		$('viewcart_button').hide();
		$('ajc_btn_checkout').show();
		$('ajax_content').show();
		$$('#ajax_image').each(function (el){
			el.innerHTML = html.msg;
		});
		if(em_ajc_currentpage == 1){window.location.href = em_ajc_baseurl+'checkout/cart/';return true;}

		if(html.check == 'success'){
			/*$$('.top-link-cart').each(function (el){
				el.innerHTML = html.toplink;
				el.title = html.toplink;
			});

			$$('.block-cart').each(function (el){
				var newElement = new Element('div');
				newElement.update(html.sidebar);
				var div = newElement.select('div')[0];
				el.update(div.innerHTML);
			});*/
			$$('.dropdown-cart').each(function(el) {
				var newElement = new Element('div');
				newElement.update(html.sidebar);
				var div = newElement.select('div')[0];
				el.update(div.innerHTML);
			});

			if(html.w_check == 1){
				var sub	=	html.w_sub;

				if($$('.add-to-cart .btn-cart').length > 0)	$$('.add-to-cart .btn-cart')[0].remove();
				if($$('.add-to-cart .paypal-logo').length > 0)	$$('.add-to-cart .paypal-logo')[0].remove();
				var tmp_whish	=	$$('.add-to-cart')[0].innerHTML;
				$$('.add-to-cart')[0].update(sub.text+tmp_whish);

				if(sub.sidebar == ""){
					if($$('.block-wishlist').length > 0)
						$$('.block-wishlist')[0].remove();
				}else{
					if($$('.block-wishlist').length > 0){
						$$('.block-wishlist').each(function (el){
							var newElement = new Element('div');
							newElement.update(sub.sidebar);
							var div = newElement.select('div')[0];
							el.update(div.innerHTML);
						});
					}
				}

				var $$li = $$('.header .links li');
				if ($$li.length > 0) {
					$$li.each(function(li) {
						 var a = li.down('a');
						 var title	=	a.readAttribute('title');
						if(title.search("ishlist") > 0){
							a.setAttribute("title", sub.link);
							a.update(sub.link);
						}
					});
				}
			}
		}
		deleteItem();

		// Auto close box ajaxcart . var em_ajc_time in template/em_ajaxcart/em_head.phtml
		if (em_ajc_time > 0){
			count = em_ajc_time;
			jQuery('.emajc_num').html('&nbsp;&nbsp;('+count+')');
			em_ajc_counter=setInterval(em_ajc_timer, 1000);
		}
	}

	function em_ajc_timer(){
		count=count-1;
		if (count <= 0){
			clearInterval(em_ajc_counter);
			//em_ajc_counter ended, do something here
				em_box.close();
				$('ajax_image').update('');
				$('ajax_loading').show();
				$('closeLink').hide();
				$('ajc_btn_checkout').hide();
				jQuery('.emajc_num').html('');
			return;
		}
		//Do code for showing the number of seconds here
			jQuery('.emajc_num').html('&nbsp;&nbsp;('+count+')');
	}

	// pre-submit callback
	function showRequest(formData, jqForm, options) {
		em_box.open();
		return true;
	}

	// post-submit callback
	function showResponse(responseText, statusText, xhr, $form)  {
		if(responseText.options == 1)
			changeOptions(responseText);
		else
			changeHTML(responseText);

		if($('product_addtocart_form')){
			var url	=	$('product_addtocart_form').readAttribute('action');
			var link = url.replace('ajaxcart','checkout').replace('ajaxcart/wishlist','wishlist/index');
			$('product_addtocart_form').setAttribute("action", link);
		}
	}

	function ajcLocation(url){
		var opt		=	url.search("options=cart");
		if(opt > 0){		// Use less than 1.9.0.0
			var ind = url.substr(0,opt-1);
			ind = ind.replace(em_ajc_baseurl,'');
			param ='ajax_option=' + ind;
			ajax_option(param);
		}else{
			ind = url.replace(em_ajc_baseurl,'');
			param ='ajax_option=' + ind;
			ajax_option(param);
		}
	}

	function setLocation(url){
		if(check_url(url) == 1) {window.location.href = url;return false;}
		var opt		=	url.search("options=cart");
		if(opt > 0){		// Use less than 1.9.0.0
			var ind = url.substr(0,opt-1);
			ind = ind.replace(em_ajc_baseurl,'');
			param ='ajax_option=' + ind;
			ajax_option(param);
		}else{
			var tmp		=	url.search("checkout/cart/");
			if(tmp > 0)	ajax_add(url);
			else{
				if(em_ajc_currentpage == 1){window.location.href = url;return true;}
				ind = url.replace(em_ajc_baseurl,'');
				param ='ajax_option=' + ind;
				ajax_option(param);
			}
		}
	}

	function check_url(url){
		var att = 0;
		if(em_ajc_enable == 1){att = 1;}
		var arr	=	[
			"checkout/onepage/",
			"wishlist/index/",
			"dir=",
			"limit=",
			"multishipping"
		];
		jQuery.each( arr, function( i, value ) {
			var redir = url.search(value);		// link setLocation not btn cart
			if(redir > 0){att = 1}
		});
		return att;
	}

	document.observe("dom:loaded", function() {
		if(em_ajc_enable == 1) return false;

		var containerDiv = $('containerDiv');	//	create container ajaxcart
		if(containerDiv)
			em_box = new LightboxAJC(containerDiv);
		var options = {
			beforeSubmit:  showRequest,
			success:       showResponse,
			dataType: 'json'
		};

		if(em_box){
			$$('button.btn-cart').each(function(el){
				if(el.up('form#product_addtocart_form')){
					var url	=	$('product_addtocart_form').readAttribute('action');
					var link = url.replace('checkout','ajaxcart').replace('wishlist/index','ajaxcart/wishlist');
					el.onclick = function(){
						if(productAddToCartForm.submit){
							var emajcForm = productAddToCartForm;
							if (('undefined' != typeof productAddToCartFormOld) && productAddToCartFormOld)
								emajcForm	=	productAddToCartFormOld;
							if(emajcForm.validator && emajcForm.validator.validate()){
								jQuery('#product_addtocart_form').ajaxForm(options);
								$('product_addtocart_form').setAttribute("action", link);
								jQuery('#product_addtocart_form').submit();
							}
						}
						return false;
					}
				}
				if(el.up('form#wishlist-view-form')){
					el.onclick = function(){
						var form = $('wishlist-view-form');
						var dir_up	=	el.up('#wishlist-table tr');
						var str	=	dir_up.readAttribute('id');
						var itemId	=	str.replace("item_","");
						addWItemToCart(itemId);
					}
				}
				if(el.up('form#reorder-validate-detail')){
					el.onclick = function(){
						var url	=	$('reorder-validate-detail').readAttribute('action');
						var param	=	$('reorder-validate-detail').serialize()
									+ '&ajax_package_name=' + em_ajc_package_name
									+ '&ajax_layout=' + em_ajc_layout
									+ '&ajax_template=' + em_ajc_template
									+ '&ajax_skin=' + em_ajc_skin;

						if(param.search("ajaxcart") < 0){
							if(reorderFormDetail.submit){
								if(reorderFormDetail.validator && reorderFormDetail.validator.validate()){
									ajax_add(url,param);
								}
								return false;
							}
						}
					}
				}
				if(em_ajc_currentpage == 1){
					var url	=	el.readAttribute('onclick');
					var tmp	=	url.search("checkout/cart/");
					if(tmp < 0)	{
						var str = url.replace('setLocation','ajcLocation');
						el.setAttribute("onclick", str);
					}
				}
			});
		}
		deleteItem();
		Event.observe('closeLink', 'click', function () {
			clearInterval(em_ajc_counter);
			em_box.close();
			$('ajax_image').update('');
			$('ajax_loading').show();
			$('closeLink').hide();
			$('viewcart_button').hide();
			$('ajc_btn_checkout').hide();
			jQuery('.emajc_num').html('');
		});
	});

	function deleteItem(){
		$$('a').each(function(el){
			if(el.href.search('checkout/cart/delete') != -1 && el.href.search('javascript:ajax_del') == -1){
				el.href = 'javascript:ajax_del(\'' + el.href +'\')';
			}
			if(el.up('.truncated')){
				var a	=	el.up('.truncated');
				a.observe('mouseover', function() {
					a.down('.truncated_full_value').addClassName('show');
				});
				a.observe('mouseout', function() {
					a.down('.truncated_full_value').removeClassName('show');
				});
			}
		});
	}

	function ajax_del(url){
		var check	=	$('shopping-cart-table');
		if(check){
			window.location.href =	url;
		}else{
			var tmp	=	url.search("checkout/cart/");
			var baseurl		=	url.substr(0,tmp);
			var tmp_2	=	url.search("/id/")+4;
			var tmp_3	=	url.search("/uenc/");
			var id		=	url.substr(tmp_2,tmp_3-tmp_2);
			var link	=	baseurl+'ajaxcart/index/delete/id/'+id;
			em_box.open();
			new Ajax.Request(link, {
				onSuccess: function(data) {
					var html = data.responseText.evalJSON();

					/*$$('.top-link-cart').each(function (el){
						el.innerHTML = html.toplink;
						el.title = html.toplink;
					});

					$$('.block-cart').each(function (el){
						var newElement = new Element('div');
						newElement.update(html.sidebar);
						var div = newElement.select('div')[0];
						el.update(div.innerHTML);
					});*/
					$$('.dropdown-cart').each(function(el) {
						var newElement = new Element('div');
						newElement.update(html.sidebar);
						var div = newElement.select('div')[0];
						el.update(div.innerHTML);
					});

					em_box.close();
					deleteItem();
				}
			});
		}

	}

	function validateDownloadableCallback(elmId, result) {
		container = $('downloadable-links-list');
		if (result == 'failed') {
			container.removeClassName('validation-passed');
			container.addClassName('validation-failed');
		} else {
			container.removeClassName('validation-failed');
			container.addClassName('validation-passed');
		}
	}

	function validateOptionsCallback(elmId, result) {
        var container = $(elmId).up('ul.options-list');
        if (result == 'failed') {
            container.removeClassName('validation-passed');
            container.addClassName('validation-failed');
        } else {
            container.removeClassName('validation-failed');
            container.addClassName('validation-passed');
        }
    }

	function ajaxaddnext(){
		if(ajcAddToCartForm.validator && ajcAddToCartForm.validator.validate()){
			var options = {
				beforeSubmit:  showRequestOptions,
				success:       showResponse,
				dataType: 'json'
			};
			jQuery('#ajc_addtocart_form').ajaxForm(options);
			jQuery('#ajc_addtocart_form').submit();
		}
	}

	// pre-submit callback
	function showRequestOptions(formData, jqForm, options) {
		$('ajax_loading').show();
		$('closeLink').hide();
		$('viewcart_button').hide();
		return true;
	}

	function ajax_changeQty(val) {
		qty = parseInt($('qty-ajax').value);
		if ( !isNaN(qty) ) {
			qty = val ? qty+1 : (qty>1 ? qty-1 : 1);
			$('qty-ajax').value = qty;
		}
	}