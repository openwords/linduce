﻿<?php 
date_default_timezone_set('Poland');
echo "ostatnia zmiana: ".date("Ymd_Hi")." <br> \n";

//$sheetImages = fopen("./obrazki_".date("Ymd_Hi").".csv", "w+");
$sheet2k = fopen("3.csv", "r");
$sheet2k_corr = fopen("3_corrected.csv", "w+");

//$columnHeaders = array("sku","image","small_image","thumbnail_image","media_gallery");

//Prepare column headers
$columnHeaders2k = fgetcsv($sheet2k);
//$columnHeaders2k = str_replace("sku", "type", $columnHeaders2k);
//$columnHeaders2k = str_replace("#", "sku", $columnHeaders2k);
$columnHeaders2k = str_replace("price3", "price", $columnHeaders2k);
array_push($columnHeaders2k, "image","small_image","thumbnail_image","media_gallery");

fputcsv($sheet2k_corr, $columnHeaders2k);

//Prepare directories and filenames
correctDirectoriesNames();
correctFilesNames();

//Get images array
$productsImages = getImagesList();

addImagesToSheet($sheet2k, $productsImages, $sheet2k_corr);

//writeArrToFile($products, $sheetImages);

//fclose($sheetImages);
fclose($sheet2k_corr);
fclose($sheet2k);

######################## FUNKCJE #################################
function addImagesToSheet($sheetToRead, $productsImages, $sheetToWrite){
	
	$i = 2;
	while (($productData = fgetcsv($sheetToRead)) !== false) {
		$sku = trim($productData[0]);
		
		//Sku proccessing
		$sku = str_replace(" ","",$sku);	//deleting spaces 
		$sku = str_replace("_","",$sku);	//deleting underscore 
		
		preg_match ( "/[^a-zA-Z\d:]/" , $sku, $matches);
			if(count($matches) != 0 ){
				foreach($matches as $match)
					$sku = str_replace("$match","",$sku);
					//echo "Trafienie: ".$match."\n";
			}
		
		$isDirectoryFound = false;
		$index = array_search($sku, array_column($productsImages, 'sku'));
		
		if($index !== FALSE){
			$nextIndexInRow = count($productData);		// 1 index after last element
			$productData[$nextIndexInRow++] = $productsImages[$index]["image"];
			$productData[$nextIndexInRow++] = $productsImages[$index]["small_image"];
			$productData[$nextIndexInRow++] = $productsImages[$index]["thumbnail_image"];
			$productData[$nextIndexInRow++] = $productsImages[$index]["media_gallery"];
		}else{
			echo "Nie znaleziono pasujacego folderu dla sku: ".$sku." linia: $i\n";
		}
		
		$productData[0] = $sku;
		//$productData[1] = "simple";
		
		########### PRICE ##################
		$productData[3] = str_replace(",",".",$productData[3]);
		$price = floatval($productData[3])/3.50;
		
		if($price > 0 && $price <= 10)
		{
			$price *= 3.0;	
		}
		else if($price >= 11 && $price <= 20)
		{
			$price *= 2.5;
		}
		else if($price >= 21 && $price <= 30)
		{
			$price *= 2.2;
		}
		else if($price >= 31)
		{
			$price *= 1.8;
		}
		$productData[3] = number_format ( floatval($price), 2 , ".", " ");
		
		
		writeRowToFile($productData, $sheetToWrite);
		$i++;
	}
}

function correctDirectoriesNames(){
	$directories = glob('*' , GLOB_ONLYDIR);
	$directoriesToCorrect = array();
	
	foreach($directories as $singleDirectory){
		if(strpos($singleDirectory , ' ') !== false || strpos($singleDirectory , '_') !== false){
			$directoriesToCorrect[] = $singleDirectory;
		}
	}
	
	echo "Katalogow do poprawienia: ".count($directoriesToCorrect)."\n";
	
	foreach($directoriesToCorrect as $directoryBadName){
		$directoryNewName = str_replace(" ","",$directoryBadName);					//deleting spaces 
		$directoryNewName = str_replace("_","",$directoryNewName);					//deleting underscore 
		
		if(!file_exists($directoryNewName) )
		{
			if(mkdir($directoryNewName))
				echo "Stworzono katalog: ".$directoryNewName."\n";
			else
				echo "Tworzenie katalogu ".$directoryNewName." zakonczone bledem\n";
		}
		
		
		foreach(array_filter(glob("$directoryBadName/*"), 'is_file') as $imagePath)
		{
			$pathArr = explode("/",$imagePath);
			$image = $pathArr[1];
			
			if (!copy("$directoryBadName/$image", "$directoryNewName/$image")){
				echo "\t Kopiowanie pliku nie powiodlo sie "."$directoryBadName/$image";
			}else{
				echo "\t Kopiowanie pliku: "."$directoryBadName/$image do $directoryNewName/$image \n";
			}
			
			
		}
		
		rrmdir($directoryBadName);
	}
}

function correctFilesNames(){
	$directories = glob('*' , GLOB_ONLYDIR);
	
	foreach($directories as $singleDirectory){
		foreach(array_filter(glob("$singleDirectory/*"), 'is_file') as $imagePath){
			
			$pathArr = explode("/",$imagePath);
			$oldFileName = $pathArr[1];
			
			//Delete dots from filename
			$imageNamePieces = explode(".",$pathArr[1]);
			$numbOfPieces = count($imageNamePieces);
			$image = "";
			
			for($i = 0; $i < $numbOfPieces-1 ;$i++){
				$image .= $imageNamePieces[$i];
			}
			$image .= ".".$imageNamePieces[$numbOfPieces-1];
			
			//Delete spaces 
			$image = str_replace(" ","",$image);	
			
			//Delete underscore
			$image = str_replace("_","",$image);	
			
			//Delete (1)
			$image = str_replace("(1)","",$image);
			
			if(strpos($image , 'ó') !== false )
				echo "jest pytajnik";
			
			preg_match ( "/[^a-zA-Z\d.:]/" , $image, $matches);
			if(count($matches) != 0 ){
				foreach($matches as $match)
					$image = str_replace("$match","",$image);
					//echo "Trafienie: ".$match."\n";
			}
			
			if($image != $oldFileName){
				if (!copy("$singleDirectory/$oldFileName", "$singleDirectory/$image")){
					echo "\t Zmiana nazwy obrazu nie powiodla sie "."$singleDirectory/$image";
				}else{
					echo "\t Zmiana nazwy obrazu: "."$singleDirectory/$oldFileName do $singleDirectory/$image \n";
					if(unlink("$singleDirectory/$oldFileName")){
						echo "\t \t Usunieto stary obraz \n";
					}
				}
			}
		}
	}	
}

function writeRowToFile($row, $sheetPointer)
{
        fputcsv($sheetPointer, $row);
}

function getImagesList(){ 
	$products = array(array());
	$i = 0;
	$directories = glob('*' , GLOB_ONLYDIR);

	for($i=0, $count = count($directories); $i < $count ;$i++ ){ 

		$directory = $directories[$i];
		$products[$i]['sku'] = "$directory";
		$products[$i]['image'] = "";
		$products[$i]['small_image'] = "";
		$products[$i]['thumbnail_image'] = "";
		$products[$i]['media_gallery'] = "";
		
		$isFirst = true;
		
		foreach( array_filter(glob("$directory/*"), 'is_file') as $image){
			if($isFirst){
				$products[$i]['image'] = "$image";
				$products[$i]['small_image'] = "$image";
				$products[$i]['thumbnail_image'] = "$image";
				$isFirst = false;
			}
			
			$products[$i]['media_gallery'] .= "$image;";
		}
	}
	
	return $products;
}

//Recursively remove dir
function rrmdir($dir) { 
   if (is_dir($dir)) { 
     $objects = scandir($dir); 
     foreach ($objects as $object) { 
       if ($object != "." && $object != "..") { 
         if (is_dir($dir."/".$object))
           rrmdir($dir."/".$object);
         else
           unlink($dir."/".$object); 
       } 
     }
     rmdir($dir); 
   } 
 }
?>